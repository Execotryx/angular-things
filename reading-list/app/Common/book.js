function Book(name, author, url)
{
	this._allowedProperties = ["name", "author", "url", "description"];
	this.setValue({"name": name, "author": author, "url": url, "description" : description});
}

Book.prototype = 
{
	get allowedProperties()
	{
		return this._allowedProperties;
	},
	set allowedProperties(value)
	{
		this._allowedProperties = value;
	},
	get fullName()
	{
		return this.author + " - " + this.name;
	}
}

Book.prototype.setValue = function(values) 
{
	for (var i = 0, key = this.allowedProperties[i]; i < this.allowedProperties.length; i++, key = this.allowedProperties[i]) 
	{
		if (values.hasOwnProperty(key)) 
		{
			this[key] = values[key];
		}
	}
};