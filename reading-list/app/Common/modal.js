Modal.prototype.initializeOn = function(image) {
	this.ModalOverlay = "active";
	this.ModalWindow = "active";
	this.ResidedImage = image;
};

Modal.prototype.destruct = function() {
	this.ModalNaming.remove();
	this.ResidedImage.remove();
	this.ModalWindow.remove();
	this.ModalOverlay.remove();
};

Modal.prototype = 
{
	//--Container for modal---
	get ModalWindow()
	{
		return this.modal;
	},
	set ModalWindow(state)
	{
		if (!this.modal) 
		{
			this.modal = document.createElement("div");
			this.modal.classList.add("modal");
			document.body.appendChild(this.modal);
		} 
		this.modal.classList.add(state);
	},
	//--Overlay-------------
	get ModalOverlay()
	{
		return this.overlay;
	},
	set ModalOverlay(state)
	{
		var _window = this;
		if (!this.overlay) 
		{
			this.overlay = document.createElement("div");
			this.overlay.classList.add("modal-overlay");
			document.body.appendChild(this.overlay);	
			this.overlay.addEventListener("click", function(event)
			{
				_window.modal.classList.remove("active");
				this.classList.remove("active");
			});		
		} 
		this.overlay.classList.add(state);
	},
	//--Image inside modal----
	get ResidedImage()
	{
		return this.nestedImage;
	},
	set ResidedImage(newImage)
	{
		if (this.ModalWindow) 
		{
			if (!this.nestedImage) 
			{
				this.nestedImage = document.createElement("img");
				this.nestedImage.classList.add("cover-zoomed");
				this.ModalWindow.appendChild(this.nestedImage);
			}
			this.nestedImage.src = newImage.src;
			this.ModalNaming = newImage.alt;
		} 
	},
	//--A name for modal-----
	get ModalNaming()
	{
		return this.modalNaming;
	},
	set ModalNaming(naming)
	{
		if (!this.modalNaming) 
		{
			this.modalNaming = document.createElement("div");
			this.modalNaming.classList.add("modal-naming");
			this.ModalWindow.appendChild(this.modalNaming);
		}
		this.modalNaming.innerText = naming;
	},

	//--Initialize---------
	initializeOn: function(image) 
	{
		this.ModalOverlay = "active";
		this.ModalWindow = "active";
		this.ResidedImage = image;
	}
};

function Modal(image) 
{
	this.overlay = document.querySelector(".modal-overlay");
	this.modal = document.querySelector(".modal");
	this.nestedImage = document.querySelector(".cover-zoomed");
	this.modalNaming = document.querySelector(".modal-naming");
	this.initializeOn(image);
}

