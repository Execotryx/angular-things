(function()
	{
		'use strict';

		// Declare app level module which depends on views, and components
		var readingListModule = angular.module('readingList', [
		  'ngRoute',
		  'ngResource',
		  'readingList.tech',
		  'readingList.view2',
		  'readingList.version'
		]);

		readingListModule.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
		  $locationProvider.hashPrefix('!');
		  $routeProvider.otherwise({redirectTo: '/tech'});
		}]);

	})();