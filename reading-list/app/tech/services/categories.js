angular.module("readingList.tech")
.factory("Categories", ["$resource", function($resource){
		return $resource("tech/sources/categories.json");
}]);