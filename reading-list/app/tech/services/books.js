angular.module("readingList.tech")
.factory("Books", ["$resource", function($resource){
		return $resource("tech/sources/books.json");
}]);