angular.module("readingList.tech")
.factory("Template", ["$templateRequest", function($templateRequest){
	return {
		get: function(directiveName) {
			return $templateRequest("template/" + directiveName + ".html");
		}
	};
}]);