angular.module("readingList.tech")
.factory("Pages", function()
{
    this.currentElementIndex = 0;
    this.totalElementsCount = 0;

    this.getCurrentElementIndex = function()
    {
        return this.currentElementIndex;
    };

    this.fetchNext = function()
    {
          this.setCurrentElementIndex(this.currentElementIndex + 1);
    };
    this.fetchPrevious = function()
    {
          this.setCurrentElementIndex(this.currentElementIndex - 1);
    };

    this.setCurrentElementIndex = function(value)
    {
        var temp = value;
        if (temp < 0)
        {
            do
            {
                temp += this.totalElementsCount;
            }
            while (temp < 0);
        }
        this.currentElementIndex = temp % this.totalElementsCount;
    };
    this.setTotalElementsCount = function(value)
    {
        this.totalElementsCount = value;
    };

    this.setDefault = function(options)
    {
        this.setTotalElementsCount(options.totalElementsCount);
        this.setCurrentElementIndex(options.currentElementIndex);
    };


    return this;
});
