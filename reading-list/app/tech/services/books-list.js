angular.module("readingList.tech")
.factory("BooksList", ["Books", "Pages", "Categories", "$interval", function(Books, Pages, Categories, $interval)
{
    var _currentElement = {},
        _receivedData = [],
        _categories = [];

    this.getCurrentElement = function()
    {
        _currentElement = _receivedData[Pages.getCurrentElementIndex()].toJSON();
        return _currentElement;
    };
    this.setCurrentElement = function(newValue)
    {
        Pages.setCurrentElementIndex(_receivedData.indexOf(newValue));
        _currentElement = _receivedData[Pages.getCurrentElementIndex()].toJSON();
    };

    this.setByIndex = function(index)
    {
        Pages.setCurrentElementIndex(index);
        _currentElement = _receivedData[Pages.getCurrentElementIndex()].toJSON();
    }

    _receivedData = Books.query(function()
    {
      Pages.setDefault({ currentElementIndex: 0, totalElementsCount: _receivedData.length });
    });

    this.getCategories = function()
    {
        var temp = Categories.query(function()
        {
            for (var i = 0; i < temp.length; i++) 
            {
                _categories[i] = { "name": temp[i], "selected": false };
            }
        });
        return _categories;
    }

    this.getData = function()
    {
        return _receivedData;
    }

    return this;
}]);
