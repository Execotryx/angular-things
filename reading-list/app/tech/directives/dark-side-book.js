angular.module("readingList.tech")
.directive("darkSideBook", function()
	{
		var darkSideBook = 
		{
			restrict: "E",
			scope: { element: "=" },
			require: "^darkSideKnowledge",

			link: function(scope, element, attrs, controller)
			{
				scope.setCurrent = function(el)
				{
					controller.setCurrentElement(el);
				}
			},

			templateUrl: "template/dark-side-book.html"
		};
		return darkSideBook;
	});
