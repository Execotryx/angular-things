angular.module("readingList.tech")
.directive("categorySelector", function()
{
	var categorySelector = 
	{
		controller: ["$scope", "BooksList", function($scope, BooksList)
		{
			$scope.categories = BooksList.getCategories();
			$scope.selectCategory = function(category)
			{
				for (var i = 0; i < $scope.categories.length; i++) 
				{
					$scope.categories[i].selected = false;
				}
				category.selected = true;
				$scope.selectedCategory = category.name;
			}
		}],
		restrict: "E",
		scope: 
		{
			selectedCategory: "=categoryCriterion"
		},
		templateUrl: "template/category-selector.html"
	};
	return categorySelector;
});