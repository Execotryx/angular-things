angular.module("readingList.tech")
.directive("lister", function()
	{
		var directive = 
		{
			controller: ["$scope", "Pages", "$interval", function($scope, Pages, $interval)
			{
				$scope.fetchNext = function()
				{
					Pages.fetchNext();
					//angular.element(zoomed).addClass(".intermission");
				};

				$scope.fetchPrevious = function()
				{
					Pages.fetchPrevious();
					//angular.element(zoomed).addClass(".intermission");
				};
			}],

			transclude: true,
			link: function(scope, element)
			{
				console.log("Viewport: " + document.documentElement.clientWidth + "x" + document.documentElement.clientHeight);
			},

			restrict: "E",
			templateUrl: "template/lister.html"
		};

		return directive;
	});
