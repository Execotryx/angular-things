angular.module("readingList.tech")
.directive("zoomedContent", function()
{
	var directive = {};

	directive.scope =
	{
		content: "="
	};

	directive.restrict = "E";
	directive.templateUrl = "template/zoomed-content.html";

	return directive;
});
