angular.module("readingList.tech")
.directive("darkSideKnowledge", function()
{
	var darkSideKnowledge = 
	{
		controller: ["$scope", "BooksList", "$interval", function($scope, BooksList, $interval)
		{
			$scope.currentElement = {};
			$scope.categoryCriterion = "";
			$scope.active = false;

			$scope.darkside_knowledge = BooksList.getData();
			$interval(function()
			{
				$scope.currentElement = BooksList.getCurrentElement();
			}, 1000);

			this.setCurrentElement = function(element)
			{
				$scope.currentElement = BooksList.setCurrentElement(element);
				$scope.active = true;
			};

			this.getCurrentElement = function()
			{
				return $scope.currentElement;
			};

			this.getActive = function()
			{
				return $scope.active;
			};

			return this;
		}],

		restrict: "E",
		templateUrl: "template/dark-side-knowledge.html"
	};
	return darkSideKnowledge;
});
