angular.module("readingList.tech")
.directive("modal", function()
{
  var modal = 
  {
  	controller: ["$scope", function($scope)
  	{
  		$scope.activeModal = $scope.active || false;
  		$scope.close = function() 
  		{ 
  			$scope.activeModal = false; 
  		};
  	}],
  	restrict: "E",
  	templateUrl: "template/modal.html",
  	transclude: true
  };
  
  return modal;
});
