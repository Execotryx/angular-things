angular.module("readingList.tech")
.directive("markAs", ["$compile", "Template", function($compile, Template)
{
  var markAs = 
  {
    restrict: "A",
    scope: {element: "="},

    controller: function($scope)
    {
        $scope.buttons = {postphone: "Postphone", inProgress: "In Progress", complete: "Complete"};
        $scope.state = {postphone: false, inProgress: false, complete: false}
        $scope.handleClick = function(button)
        {
            $scope.element.isPostphoned = (button === $scope.buttons.postphone);
            $scope.element.isInProgress = (button === $scope.buttons.inProgress);
            $scope.element.isCompleted = (button === $scope.buttons.complete);
        };
    },

    link: function($scope, $element)
    {
      Template.get("mark-as").then(function(template)
      {
        $element.append($compile(template)($scope));
      });
    }
  };

  return markAs;
}]);
