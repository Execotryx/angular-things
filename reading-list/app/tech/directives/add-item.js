angular.module("readingList.tech")
.directive("addItem", function()
{
  var addItem = 
  {
    scope: true,
    controller: ["$scope", function($scope)
    {
        $scope.newItem =
        {
            name: "",
            author: "",
            url: "",
            description: ""
        };

        var resetNewItem = function()
        {
          $scope.newItem =
          {
              name: "",
              author: "",
              url: "",
              description: ""
          };          
        }

        $scope.addNewElement = function()
        {
          $scope.activeModal = true;
        };

        $scope.ok = function()
        {
          $scope.activeModal = false;
          resetNewItem();
        };

        $scope.cancel = function()
        {
          $scope.activeModal = false;
          resetNewItem();
        };
    }],

    restrict: "E",
    templateUrl: "template/add-item.html"
  };
  return addItem;
});
