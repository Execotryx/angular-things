angular.module("readingList.tech")
.directive("canZoom", ['$compile', 'Template', function($compile, Template)
{
  var canZoom = 
  {
    restrict: "A",

    scope: { current: "=", active: "=", index: "=" },

    controller: ["$scope", function($scope)
    {
        $scope.close = function() 
        { 
          $scope.activeModal = $scope.active = false; 
        };

        $scope.setActiveState = function(state) 
        { 
          $scope.activeModal = $scope.active = state; 
        };
        $scope.setCurrentElement = function(element) { $scope.current = element; };

        $scope.$watch(function()
          {
            return $scope.active;
          }, function(newValue, oldValue)
          {
            $scope.activeModal = $scope.active;
          });

        $scope.$watch(function()
          {
            return $scope.activeModal;
          }, function(newValue, oldValue)
          {
            $scope.active = $scope.activeModal;
          });
    }],

    require: "^darkSideKnowledge",

    link: function(scope, element, attrs, controller)
    {
      scope.setActiveState(false);
      Template.get("can-zoom").then(function(template)
      {
        element.append($compile(template)(scope));
      });
    }
  };

  return canZoom;
}]);
