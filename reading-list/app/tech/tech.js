(function()
{
	'use strict';

	angular.module('readingList.tech', ['ngRoute'])
	.config(['$routeProvider', function($routeProvider) {
	  $routeProvider.when('/tech', {
	    templateUrl: 'tech/tech.html',
	  });
	}])
})();
