angular.module("readingList.tech")
.filter("category", function()
{
	return function(input, category)
	{
		if (category !== "" && category !== "All") 
		{
			var out = [];

			angular.forEach(input, function(element)
			{
				if (element.category.indexOf(category) != -1) 
				{
					out.push(element);
				}
			});
			return out;
		}
		return input;
	};	
});